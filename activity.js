{
	"Users" : 
	[
		{
		"userID" : 1, 
		"First Name" : "Jason",
		"Last Name" : "Smith",
		"Email" : "jasonsmith@mail.com",
		"Password" : "swordfish",
		"isAdmin" : false,
		"Mobile Number" : "09171234567"
		},
		{
		"userID" : 2, 
		"First Name" : "Jane",
		"Last Name" : "Wilson",
		"Email" : "janewilson@mail.com",
		"Password" : "banana",
		"isAdmin" : false,
		"Mobile Number" : "09171112233"
		}
	],

	"Orders" :
	[
		{
		"userID" : 1,
		"Transaction Date" : "01/01/2023",
		"Status" : "Paid",
		"Total" : 1000
		},
		{
		"userID" : 2,
		"Transaction Date" : "03/03/2023",
		"Status" : "Pending",
		"Total" : 5000
		}

	],

	"Products" :
	[
		{
			"productID" : 1,
			"Name" : "Wireless Mouse",
			"Description" : "Wireless Bluetooth Mouse Model v2.0 batteries not included.",
			"Price" : 1000,
			"Stocks" : 10,
			"isActive" : true,
			"SKU" : "ZG012ABA"
		},
		{
			"productID" : 2,
			"Name" : "Wireless Keyboard",
			"Description" : "Wireless Bluetooth Keyboard Model v3.5 batteries not included.",
			"Price" : 4000,
			"Stocks" : 3,
			"isActive" : true,
			"SKU" : "XA033ABV"
		}
	],

	"Order Products" :
	[
		{
			"orderID" : 1,
			"productID": [1],
			"Quantity" : [1],
			"Price" : [1000],
			"subTotal" : 1000
		},
		{
			"orderID" : 2,
			"productID": [1,2],
			"Quantity" : [1,1],
			"Price" : [1000,4000],
			"subTotal" : 5000
		}
	]
}